# -*- coding:utf-8 -*-
import urllib2
import re


class QSBK:
    def __init__(self):
        self.pageindex = 1
        self.headers = {'User-Agent': 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)'}
        self.stories = []
        self.enable = False
        self.baseurl = 'http://www.qiushibaike.com/hot/page/'
        self.n = 0

    def getPage(self, pageindex):
        request = urllib2.Request(self.baseurl + str(pageindex), headers=self.headers)
        response = urllib2.urlopen(request)
        return response.read().decode('utf-8')

    def getStories(self, pageindex):
        content = self.getPage(pageindex)
        pattern = re.compile(
            'author-text\',\'chick\']\)\">\n<h2>\n(.*?)\n</h2>\n</a>.*?<div class="content">\n<span>(.*?)</span>.*?class=\"stats-vote\"><i class=\"number\">(.*?)</i>.*?list-comment\',\'chick\']\)\">\n<i class=\"number\">(.*?)</i>',
            re.S)
        items = re.findall(pattern, content)
        for item in items:
            self.n += 1
            story = "author:  " + item[0] + "\n" + "content:\n" + re.sub(re.compile('<br/>'), "\n",
                                                                         item[1]).strip() + "\nlike:  " + item[
                        2] + "\ncomments:  " + item[3]
            self.stories.append("---------" + str(self.n) + "---------\n" + story)

    def start(self, maxpage):
        printer = WriteFile()
        for i in range(1, maxpage + 1):
            self.getStories(i)
        for j in range(0, self.stories.__len__()):
            printer.doWrite(self.stories[j]+"\n")
        printer.closeFile()


class WriteFile:
    def __init__(self):
        self.filePath = '/Users/xcli/Desktop/Demo.txt'
        self.qsbkFile = open(self.filePath, "w+")

    def doWrite(self, str):
        self.qsbkFile.write(str.encode('utf-8'))

    def closeFile(self):
        self.qsbkFile.close()


spider = QSBK()
spider.start(100)
